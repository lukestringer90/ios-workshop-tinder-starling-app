//
//  TableViewController.swift
//  MyFirstApp
//
//  Created by Luke Stringer on 10/11/2021.
//

import UIKit

class TableViewController: UIViewController {
    
    let transactions = [
        Transaction(companyName: "Just Eat", time: "22:29", amount: 11.0),
        Transaction(companyName: "Sky Bet", time: "15:33", amount: 5.00),
        Transaction(companyName: "Trainline", time: "07:33", amount: 4.20),
        Transaction(companyName: "Uber", time: "07:19", amount: 9.25)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension TableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionCell
        let transaction = transactions[indexPath.row]
        cell.configure(with: transaction)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0: return "Wednesday 10th November"
        case 1: return "Thursday 11th November"
        case 2: return "Friday 12th November"
        default: return nil
        }
    }
}

extension TableViewController: UITableViewDelegate {
    
}
